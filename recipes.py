import os.path
import itertools
from lupa import LuaRuntime
import tempfile
from zipfile import ZipFile
import argparse


class Ingredients(dict):
    def __iadd__(self, a: dict):
        for k, v in a.items():
            if k in self:
                self[k] += v
            else:
                self[k] = v
        return self

    def __mul__(self, num: int):
        ret = Ingredients()
        for ingredient in self.keys():
            ret[ingredient] = self[ingredient] * num
        return ret


class Recipe:
    def __init__(self, name, ingredients, level):
        self.name = name
        self.ingredients = ingredients
        self.level = level


def _load_recipes(scriptdir):

    recipes = {}

    with ZipFile(os.path.join(os.path.expanduser(scriptdir),
                              'scripts.zip')) as scriptsfile:
        with tempfile.TemporaryDirectory() as d:
            scriptsfile.extractall(path=d)

            os.environ['LUA_PATH'] = f'{d}/scripts/?.lua'
            lua = LuaRuntime(unpack_returned_tuples=True)

            lua.execute('''math = { pow = math.ceil,
                                    pi = math.pi,
                                    ceil = math.ceil,
                                    max = math.max }''')
            lua.require('class')
            lua.require('constants')
            lua.require('tuning')
            lua.require('recipes')

            for name, values in lua.globals().AllRecipes.items():
                ingts = itertools.chain(values.ingredients.items(),
                                        values.tech_ingredients.items(),
                                        values.character_ingredients.items())
                recipe = {}
                for i_name, i_values in ingts:
                    recipe[i_values.type] = i_values.amount
                level = [(k, v) for k, v in values.level.items() if v > 0]
                recipes[name] = Recipe(name, Ingredients(recipe), level)

    return recipes


def getrecipe(name):
    ingredients = recipes[name].ingredients
    ret = Ingredients()
    for i_name, i_amount in ingredients.items():
        if i_name in recipes:
            ret += getrecipe(i_name).ingredients * i_amount
        elif i_name in ret:
            ret[i_name] += i_amount
        else:
            ret[i_name] = i_amount
    return Recipe(name, ret, "")


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Get Don't Starve recipes.")
    parser.add_argument('items', type=str, nargs='+',
                        help='items to build')
    args = parser.parse_args()

    recipes = _load_recipes("~/.steam/steam/steamapps/common/"
                            "Don't Starve Together/data/databundles")

    ingredients = Ingredients()
    for item in args.items:
        ingredients += getrecipe(item).ingredients

    print(ingredients)
